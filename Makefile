CC=gcc
CFLAGS=-Wall -Wextra -pedantic -g

CDIR=src
HDIR=headers
C=main.c md5.c pick_folder.c
H=main.h md5.h pick_folder.h

OPENSSL_LIBS=`pkg-config --libs openssl`
CURL_LIBS=`pkg-config --libs libcurl`
GTK_FLAGS=`pkg-config --cflags gtk+-3.0`
GTK_LIBS=`pkg-config --libs gtk+-3.0`

all:
	$(CC) -o bin $(CFLAGS) $(CDIR)/*.c $(GTK_FLAGS) $(OPENSSL_LIBS) $(CURL_LIBS) $(GTK_LIBS) -rdynamic

gres:
	cd src/interface; \
	glib-compile-resources --generate-source gres.xml; \
  sed -i 's/#include\ <gio\/gio.h>/#include\ \"headers\/gres.h\"/g' gres.c; \
	mv gres.c ../
