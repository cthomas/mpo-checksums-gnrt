import os
import datetime
import hashlib

#Detect Os
import platform

#Tinker stuff
import Tkinter as tk
import tkFont
import tkFileDialog
import ttk
 
# custom import
from definitions import *
from functions import *

def pick_folder():
	folder = tkFileDialog.askdirectory()
	if not folder == "":
		fgnrt_checksums(folder)

def manual():
	win = tk.Toplevel(root)
	win.resizable(0, 0)
	win.geometry("300x500")
	win.title("# [Manual]")

	#define frame for [text area]
	frm_text = ttk.Frame(win,
                     width=300,
                     height=475)
	frm_text.place(x=0,y=0)
	frm_text.pack_propagate(0)

	# Scrollbar
	scrollbar = ttk.Scrollbar(frm_text) 
	scrollbar.pack(side=tk.RIGHT,
                   fill=tk.Y)
	
	mantext = tk.Text(frm_text,	
                      wrap=tk.WORD,
                      yscrollcommand=scrollbar.set)
	
	mantext.insert(tk.INSERT, "foo bar")
	mantext.config(state=tk.DISABLED)
	mantext.pack(fill=tk.BOTH, 
                 expand=True)

	#define frame for [btn previous]	
	frm_btn_prev = ttk.Frame(win,
                             width=s_w_btn_nrml,
                             height=s_h_btn_nrml)
	frm_btn_prev.place(x=0,y=475)
	frm_btn_prev.pack_propagate(0)
	
	#btn previous		
	btn_prev = ttk.Button(frm_btn_prev, 
                          text="previous")
	btn_prev.pack(fill=tk.BOTH,expand=True)

	#define frame for [btn previous]	
	frm_btn_next = ttk.Frame(win,
                             width=s_w_btn_nrml,
                             height=s_h_btn_nrml)
	frm_btn_next.place(x=225,y=475)
	frm_btn_next.pack_propagate(0)
	
	#btn previous		
	btn_next = ttk.Button(frm_btn_next, 
                          text="previous")
	btn_next.pack(fill=tk.BOTH,expand=True)
# Main

# Root Window Definition
root = tk.Tk()

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

root.resizable(0, 0)
root.geometry("400x600")
root.title("MPO -- [CHECKSUMMS GENERATOR]")

default_font = tkFont.nametofont("TkDefaultFont")
myfont = tkFont.Font(family="Verdana", size=10)
default_font.configure(family="Verdana", size=10)
root.option_add("*Font", default_font)

# Style definition
style = ttk.Style()
style.theme_use('clam')

fbtn = ttk.Frame(root,
                width=200,
                height=200)
                #bg="white")

fbtn.pack_propagate(0)
fbtn.place(x=100,y=200)

btn = ttk.Button(fbtn,
                text="Click Me",
                command=pick_folder)

btn.pack(fill=tk.BOTH,
        expand=1)



manframe = ttk.Frame(root,
                     width=s_w_btn_nrml,
                     height=s_h_btn_nrml)
manframe.pack_propagate(0)
manframe.place(x=325,y=575)

manbtn = ttk.Button(manframe,
                    text="Manual",
                    command=manual)

manbtn.pack(fill=tk.BOTH,
            expand=1)

root.mainloop()
