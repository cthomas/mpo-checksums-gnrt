# Functions 
def fgnrt_checksums(path):
	safe_path = path.replace("\\","/")
	fmd5 = open(safe_path + "/" + "checksums.md5", "w")
	time_now = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
	fmd5.write("#Generated " + time_now + "\n"*3)
	for root, dirs, files in os.walk(path):
		for ofile in files:
			hidden = 0
			for item in root.split(os.sep):
					if item[:1] == ".":
						hidden = 1
			if hidden == 0:
				fpath = (root + os.sep + ofile).replace("\\","/")
				fitem = open(fpath, "r")
				md5hash = hashlib.md5((fitem.read())).hexdigest()
				fitem.close()
				fmd5.write(md5hash + " *" + fpath.replace(safe_path, "")[1:] + "\n")
	fmd5.close()
