#include "headers/pick_folder.h"

G_MODULE_EXPORT \
void pick_folder()
{
  builder_ref("/org/gtk/mpo-checksums-gnrt/pick_folder.xml");
  pickfolder=\
  GTK_WIDGET\
  (\
   gtk_builder_get_object\
   (\
    builder, \
    "pickfolder" \
    )\
  );
  builder_unref();

  // SET THE FOLDER CHOOSER DIALOG PARENT
  gtk_window_set_transient_for((GtkWindow*)pickfolder, (GtkWindow*)window);

  gint result=\
  gtk_dialog_run\
  (\
    GTK_DIALOG(pickfolder)\
  );

  if(result == GTK_RESPONSE_OK)
  {
    GtkFileChooser *chooser = GTK_FILE_CHOOSER(pickfolder);
    char *foldername = gtk_file_chooser_get_filename(chooser);
    gtk_widget_hide(pickfolder);
    gtk_widget_set_sensitive(mainbtn, FALSE);
    while (gtk_events_pending ())
      gtk_main_iteration ();
    gnrt_md5_file(foldername);
  }

  gtk_widget_destroy(pickfolder);
}
