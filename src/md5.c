#include "headers/md5.h"

int prc = 1;
// returning md5 hash
char* md5(const char* path)
{
  prc = 0; // reset progressbar

  FILE* file = fopen(path, "r");
  // abort prog if file * is null
  assert(file != NULL);

  long long j, size, iter;

  // get file size for progress bar //

  #if __WIN32__ //WINDOWS SUCKS !!
    LARGE_INTEGER lpFileSize = {0};
    HANDLE hFile = CreateFile\
            (\
        path, \
        GENERIC_READ, \
        FILE_SHARE_READ, \
        NULL, \
        OPEN_EXISTING, \
        FILE_ATTRIBUTE_READONLY, \
        NULL \
            );
    assert(hFile != INVALID_HANDLE_VALUE);
    GetFileSizeEx(hFile, &lpFileSize);
    CloseHandle(hFile);
    size = lpFileSize.QuadPart;
  #else
    struct stat buf;
    stat(path, &buf);
    size = buf.st_size;
  #endif

  ////////////////////////////////////

  // calculate number of iterations
  iter = size/1024;

  unsigned char md5hash[EVP_MAX_MD_SIZE];
  EVP_MD_CTX* ctx =  EVP_MD_CTX_create();
  int bytes, mdlen, i;
  unsigned char data[1024];


  ///MD5 STUFF
  EVP_DigestInit_ex(ctx, EVP_md5(), NULL);
  for(j=0; (bytes = fread(data, 1, 1024, file)) != 0; j++)
  {
    if(iter > 10000)
    {
      if(j % (iter/10000) == 0)
        progress();
      else
      {}
    }

    EVP_DigestUpdate(ctx, data, bytes);

  }

  EVP_DigestFinal_ex(ctx, md5hash, (unsigned int*)&mdlen);
  EVP_MD_CTX_destroy(ctx);
  /////////////////////////////////////////////////////////

  fclose(file);

  char* buf = (char*)calloc(33, sizeof(char));
  for(i=0; i < mdlen; i++)
    sprintf(buf + strlen(buf), "%02x", md5hash[i]);
  return buf;
}

void progress(void)
{
  double prgs = (double)prc/10000;
  gtk_progress_bar_set_fraction((GtkProgressBar*)progressbar, prgs);
  while (gtk_events_pending ())
    gtk_main_iteration ();
  prc++;
}

void date(char* buffer)
{
  time_t timer;
  time(&timer);
  struct tm* tm_info;
  tm_info = localtime(&timer);
  strftime(buffer, 26, "%d/%m/%Y %H:%M:%S", tm_info);
}

////////////////////////////////////////////////////////////////////////////////
void rmd5_dir(char *base_abs_path, char *base_rel_path, FILE* file)
{
	char abs_path[1024];
	char rel_path[1024];
  char* _rel_path;
	char* item;

	GDir* dir;
	dir = g_dir_open(base_abs_path, 0, NULL);

	assert(dir != NULL);
  assert(file != NULL);

	while( (item = g_dir_read_name(dir)) != NULL )
	{
		if(item[0] != '.')
		{
			sprintf(abs_path, "%s/%s", base_abs_path, item);
      sprintf(rel_path, "%s/%s", base_rel_path, item);
      _rel_path = rel_path + 1;

			if(g_file_test(abs_path, G_FILE_TEST_IS_DIR))
				rmd5_dir(abs_path, rel_path, file);
			else if(strcmp(item,"checksums.md5"))
      {
        char buffer[256];
        sprintf(buffer, "Working on [%s]", item);
        gtk_label_set_text((GtkLabel*)infolbl, buffer);
        while (gtk_events_pending ())
          gtk_main_iteration ();
        char* md5hash = md5(abs_path);
        fprintf(file, "%s *%s\n", md5hash, _rel_path);
        free(md5hash);
      }
		}
	}
}
////////////////////////////////////////////////////////////////////////////////

void gnrt_md5_file(char *path)
{
  char checksums_path[1024];

  strcpy(checksums_path, path);
  strcat(checksums_path, "/checksums.md5");

  FILE* output = fopen(checksums_path, "w");
  assert(output != NULL);

  char crnt_date[26] = {0};
  date(crnt_date);

  fprintf(output, "#Generated %s\n\n\n", crnt_date);
  rmd5_dir(path ,"", output);
  fclose(output);
  gtk_label_set_text((GtkLabel*)infolbl, "[ Finished !! ]");
  while (gtk_events_pending ())
    gtk_main_iteration ();

}
