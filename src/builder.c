#include "headers/builder.h"

void builder_ref(const char* path)
{
  //////////BUILDER PART /////////////////////////////////////////////////////////
    builder = gtk_builder_new();

    gtk_builder_add_from_resource\
    (\
     builder, \
     path, \
     NULL \
    );

    gtk_builder_connect_signals\
    (\
     builder, \
     NULL \
    );
  ////////////////////////////////////////////////////////////////////////////////
}

void builder_unref(void)
{
    g_object_unref(builder);
}
