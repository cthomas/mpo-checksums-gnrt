#ifndef BUILDER_H
#define BUILDER_H

#include "declarations.h"

void builder_ref(const char* item);
void builder_unref(void);

#endif
