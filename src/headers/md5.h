#ifndef MD5_H
#define MD5_H

#include "declarations.h"

char* md5(const char* path);
void progress(void);
void date(char *buffer);
void gnrt_md5_file(char *path);

#endif
