#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include <assert.h>
#include <curl/curl.h>
#include <dirent.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#if __WIN32__
  #include <windows.h>
#else
  #include <sys/types.h>
  #include <sys/stat.h>
#endif

GtkBuilder *builder;
GtkWidget *window;
GtkWidget *pickfolder;
GtkWidget *mainbtn;
GtkWidget *aboutwin;
GtkWidget *infolbl;
GtkWidget *progressbar;
GtkCssProvider *style_provider;

#endif
