#include "headers/about.h"

G_MODULE_EXPORT \
void about()
{
  builder_ref("/org/gtk/mpo-checksums-gnrt/about.xml");
  aboutwin=\
  GTK_WIDGET\
  (\
    gtk_builder_get_object\
    (\
     builder, \
     "about" \
    )\
  );
  builder_unref();

  //gtk_window_set_transient_for((GtkWindow*)aboutwin, (GtkWindow*)window);
  gtk_window_set_transient_for((GtkWindow*)aboutwin, NULL);
  puts("OK !!");

  gtk_dialog_run\
  (\
    GTK_DIALOG(aboutwin)\
  );

  gtk_widget_destroy(aboutwin);
}
