#include "headers/main.h"

int main(int argc, char **argv)
{

  gtk_init(&argc, &argv);
  builder_ref("/org/gtk/mpo-checksums-gnrt/ui.xml");

//////////GRESOURCE PART////////////////////////////////////////////////////////
/*
GRESOURCE IS A METHOD TO STOCK ALL OUR RESOURCES IN A BINARY FORMAT
IT GENERATE C CODE WITH GLIB-COMPILE-RESOURCES, SO WE HAVE TO REGISTER
THE RESSOURCE AND AFTER THAT WE CAN USE THE /org/gtk/mpo-checksums PATH
*/
  GResource *res = gres_get_resource();
  g_resources_register(res);
////////////////////////////////////////////////////////////////////////////////

//////////CSS PART//////////////////////////////////////////////////////////////
/*
WE USE CSS TO THEME THE APPLICATION, NOT BIG MODIF, BECAUSE I TRY TO
GET STRAIGHT TO ADWAITA THEMES, I ONLY USE CSS FOR ROUNDED BUTTON OR SOME SMALL
TWEAKS.
*/
  style_provider = gtk_css_provider_new();

  gtk_css_provider_load_from_resource\
  (\
   style_provider, \
   "/org/gtk/mpo-checksums-gnrt/style.css" \
  );

  gtk_style_context_add_provider_for_screen\
  (\
   gdk_screen_get_default(), \
   (GtkStyleProvider*)style_provider, \
   GTK_STYLE_PROVIDER_PRIORITY_USER \
  );
////////////////////////////////////////////////////////////////////////////////


//////////ATTRIBUTION OF WIDGET/WINDOW//////////////////////////////////////////

  window=\
  GTK_WIDGET\
  (\
   gtk_builder_get_object\
   (\
    builder, \
    "rootwin" \
    )\
   );

  mainbtn=\
  GTK_WIDGET\
  (\
    gtk_builder_get_object\
    (\
     builder, \
     "btn_main" \
    )\
  );

  progressbar=\
  GTK_WIDGET\
  (\
    gtk_builder_get_object\
    (\
      builder, \
      "progress" \
    )\
  );

  infolbl=\
  GTK_WIDGET\
  (\
    gtk_builder_get_object\
    (\
      builder, \
      "labelinfo" \
    )\
  );

////////////////////////////////////////////////////////////////////////////////

// UNREF BUILDER WE DO NOT NEED IT ANYMORE
  builder_unref();
// SHOW THE MAIN WINDOW
  gtk_widget_show(window);
  g_signal_connect(window,"destroy",G_CALLBACK(gtk_main_quit),NULL);

// MAIN LOOP
  gtk_main();
  exit(0);
}
